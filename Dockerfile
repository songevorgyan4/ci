FROM ubuntu:bionic

RUN apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN pip3 install numpy
RUN pip3 install sklearn
RUN pip3 install wheel
RUN pip3 install setuptools
