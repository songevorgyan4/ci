import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib
from step_size import ConstantStepSize, SeqStepSize, StepSize
from IPython.display import Image 

matplotlib.use('Agg')
params = {'legend.fontsize': 20,
          'legend.handlelength': 4,
          "axes.labelsize": 45,
          "xtick.labelsize": 25,
          "ytick.labelsize": 25,
          "lines.linewidth": 4,
           "axes.titlesize":30}
matplotlib.rcParams.update(params)

###  Градиентный спуск
class GradientDescent:
    def __init__(self, StepSizeChoice, return_history=True, name=None):
        self.name = name
        self.StepSizeChoice = StepSizeChoice
        self.return_history = return_history
        self.history = []
    
    def solve(self, x0, f, gradf, tol=1e-9, max_iter=10000):
        self.history = [(x0, time.time())]
        x = x0.copy()
        k = 0
        x_prev = None
        while x_prev is None or np.linalg.norm(x-x_prev) > tol: 
            h = -gradf(x)
            alpha = self.StepSizeChoice(x, h, k, gradf, f)
            x_prev, x = x, x + alpha * h
            if self.return_history:
                self.history.append((x, time.time()))
            if k >= max_iter:
                break
            k += 1
        return x
def parse_logs(xhistory, ret_time=False, funcx=None):
    values = [funcx(x) for x, _ in xhistory]
    if ret_time:
        times = [t for _, t in xhistory]
        times = [times[ind]-times[0] for ind, t in enumerate(times)]
    else:
        times = [i for i in range(len(xhistory))]
    return times, values

class PolyakStepSize(StepSize):
    def __init__(self, f_sol=0):
        self.f_sol = f_sol
    
    def __call__(self, x, h, k, gradf, f, *args, **kwargs):
        return (f(x)-self.f_sol) / np.linalg.norm(gradf(x))**2

class PolyakStepSizeWithLowerBound(StepSize):
    def __init__(self, f_sol=0):
        self.f_sol = f_sol
    
    def __call__(self, x, h, k, gradf, f, *args, **kwargs):
        return (f(x)-self.f_sol) / (2*np.linalg.norm(gradf(x))**2)

class AdaptivePolyak:
    def __init__(self, return_history=True, name=None):
        self.name = name
        #self.StepSizeChoice = StepSizeChoice
        self.return_history = return_history
        self.history = []
    
    def solve(self, x0, f, gradf, tol=1e-9, max_iter=10000, epoch = 2000):
        self.history = [(x0, time.time())]
        x = x0.copy()
        f_sol = 0
        f_sol_prev = None
        ep = 0
        while ep <= epoch:
            method = GradientDescent(PolyakStepSizeWithLowerBound(f_sol))
            x = method.solve(x, f, gradf, tol, max_iter)
            f_sol_prev, f_sol = f_sol, (f(x) - f_sol)/2
            ep+=1
            if ep >= epoch:
                break
            if self.return_history:
                self.history.append((x, time.time()))
        return x

def func(x, A, b):
    return np.abs(A.dot(x) - b).sum()

def grad_f(x, A, b):
    y = (A.dot(x) - b)
    return A.T @ np.sign(y)
np.random.seed(1)
n = 500
m = 100
A = np.random.randn(m, n)
x_true = np.random.rand(n)
b = A.dot(x_true)
x0 = np.ones(n)
f = lambda x: func(x, A, b)
gradf = lambda x: grad_f(x, A, b)

methods = [
          GradientDescent(PolyakStepSize(0), name="GD, Polyak"),
          GradientDescent(PolyakStepSizeWithLowerBound(0), name="GD, Polyak with lower bound = 0"),
          GradientDescent(PolyakStepSizeWithLowerBound(-1), name="GD, Polyak with lower bound = -1"),
          AdaptivePolyak(PolyakStepSizeWithLowerBound(0), name="AP, Polyak"),
          ]

for method in methods:
    method.solve(x0, f, gradf, tol=1e-9, max_iter=10000)


%matplotlib inline
plt.figure(figsize=(20, 16))
funcx = lambda x: 1/2 * np.linalg.norm(A @ x -b, 1)
#funcx = lambda x: f(x)
for method in methods:
    xhistory = method.history
    times, values = parse_logs(method.history, ret_time=False, funcx=funcx)
    plt.semilogy(times, values, label=method.name)
plt.grid()
plt.ylabel(r"Function Value")
plt.xlabel(r"Number of iteration, $k$")
plt.legend()

%matplotlib inline
plt.figure(figsize=(20, 16))
funcx = lambda x: 1/2 * np.linalg.norm(A @ x -b, 1)
#funcx = lambda x: f(x)
for method in methods:
    xhistory = method.history
    times, values = parse_logs(method.history, ret_time=True, funcx=funcx)
    plt.semilogy(times, values, label=method.name)
plt.grid()
plt.ylabel(r"Function Value")
plt.xlabel(r"Время, с")
plt.legend()
