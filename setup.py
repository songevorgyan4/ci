import os
from setuptools import setup
from setuptools import find_packages

setup(
    name="polyak",
    version="1.0.0",
    packages=find_packages(),
    install_requires=["numpy"],
    description="polyak",
    long_description="polyak",
    python_requires=">=3.4"
)
